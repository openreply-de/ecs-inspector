/**
 * @file retrieves information from the ecs agent about the docker container where the current process is being executed
 * @author Florian Schaper <f.schaper@reply.de>
 * @copyright Florian Schaper 2018, MIT License
 */

'use strict';

const request = require('request-promise-native');
const fs = require('fs');
const { promisify } = require('util');

// eslint-disable-next-line security/detect-non-literal-fs-filename
const readFile = promisify(fs.readFile);

// retrieves the task id and docker id of the current docker ecs container
const taskAndDockerId = /\/ecs\/([^/]*)\/([^\n]*)/;

/**
 * @typedef {Object} ECSAgentTaskInformationPortInfo
 * @property {string} protocol - protocol used e.g. tcp
 * @property {number} container - port on the container side
 * @property {number} host - port on the host side
 */

/**
 * @typedef {Object} ECSAgentTaskInformation
 * @property {string} arn - aws arn uniquely identifying the task
 * @property {string} dockerId - docker id
 * @property {string} containerName - name of the container
 * @property {Array.<ECSAgentTaskInformationPortInfo>} ports - docker to host port mapping
 */

/**
 * retrieves information's about the running ecs task from the ecs agent
 *
 * @param {Object} [options] - configuration options
 * @param {string} options.ecsAgentHost - hostname and port of the ecs agent. defaults to `172.17.0.1:51678`
 * @param {number} options.timeout - milliseconds to wait for the discovery service to respond on each request
 * @returns {Promise<ECSAgentTaskInformation>} information about the current docker process on ecs
 */
function ecsAgentCurrentTask({ ecsAgentHost, timeout } = { ecsAgentHost: '172.17.0.1:51678', timeout: 1000 }) {
  // `/proc/1/cpuset` holds the task and dockerId for this container
  return readFile('/proc/1/cpuset', 'utf8')
    .then((ecsIds) => {
      const [, taskId, dockerId] = ecsIds.match(taskAndDockerId);
      // get the list of all running tasks on this instance
      return request.get(`http://${ecsAgentHost}/v1/threads`, { json: true, timeout }).then((response) => {
        // identify our task in the list of tasks
        // eslint-disable-next-line security/detect-non-literal-regexp
        const taskArn = new RegExp(`${taskId}$`);
        return response.Tasks.find(taskEntry => taskEntry.Arn.match(taskArn))
          || (() => { throw new Error('could not find taskId in the current set of tasks from ECS agent'); })();
      }).then((taskInformation) => {
        // find 'us' in the list of running containers for this task
        const dockerImage = taskInformation.Containers.find(container => container.DockerId === dockerId);
        return {
          arn: taskInformation.Arn,
          dockerId: dockerImage.DockerId,
          containerName: dockerImage.Name,
          ports: (dockerImage.Ports || []).map(portEntry => ({
            protocol: portEntry.Protocol,
            container: portEntry.ContainerPort,
            host: portEntry.HostPort
          }))
        };
      });
    });
}

module.exports = ecsAgentCurrentTask;
