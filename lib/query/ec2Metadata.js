/**
 * @file retrieves ec2 instance metadata for the local instance
 * @author Florian Schaper <f.schaper@reply.de>
 * @copyright Florian Schaper 2018, MIT License
 */

'use strict';

const request = require('request-promise-native');

/**
 * @typedef {Object} EC2HostInfo
 * @property {string} [publicHostname] - public dns domain name of the ec2 instance (if available)
 * @property {string} [publicIpv4] - public ipv4 address of the ec2 instance (if available)
 * @property {string} [localHostname] - private dns domain name of the ec2 instance (if available)
 * @property {string} [localIpv4] - private ipv4 address of the ec2 instance (if available)
 */

/**
 * retrieves the public and private hostnames and ip addresses for the current ec2 instance
 *
 * @param {Object} [options] - configuration options
 * @param {string} options.ec2MetatadataHost - hostname and port of the ec2 meta - data host.defaults to `169.254.169.254`
 * @param {number} options.timeout - milliseconds to wait for the discovery service to respond on each request
 * @returns {Promise<EC2HostInfo>} information about the ec2 host
 */
function ec2Metadata({ ec2MetatadataHost, timeout } = { ec2MetatadataHost: '169.254.169.254', timeout: 1000 }) {
  return Promise.all(Object.entries({
    publicHostname: 'public-hostname',
    publicIpv4: 'public-ipv4',
    localHostname: 'local-hostname',
    localIpv4: 'local-ipv4'
  }).map(([key, metadata]) => request.get(`http://${ec2MetatadataHost}/latest/meta-data/${metadata}`, { json: false, timeout })
    .then(value => ({ [key]: value }))
    .catch(() => ({})))).then(result => Object.assign(...result));
}

module.exports = ec2Metadata;
