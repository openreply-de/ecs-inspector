/**
 * @file retrieves information about the docker container where the current process is being executed
 * @author Florian Schaper <f.schaper@reply.de>
 * @copyright Florian Schaper 2018, MIT License
 */

'use strict';

const fs = require('fs');
const { promisify } = require('util');

// eslint-disable-next-line security/detect-non-literal-fs-filename
const readFile = promisify(fs.readFile);

/**
 * @typedef {Object} ECSFileTaskInformationPortInfo
 * @property {string} protocol - protocol used e.g. tcp
 * @property {number} container - port on the container side
 * @property {number} host - port on the host side
 */

/**
 * @typedef {Object} ECSFileTaskInformation
 * @property {string} arn - aws arn uniquely identifying the task
 * @property {string} cluster - name of the cluster
 * @property {string} dockerId - docker id
 * @property {string} containerName - name of the container
 * @property {Array.<ECSFileTaskInformationPortInfo>} ports - docker to host port mapping
 */

/**
 * parses and returns the ecs metadata file information
 *
 * the ecs agent has to be configured to provide an metadata file for containers
 * see the [ecs metadata file documentation](https://docs.aws.amazon.com/AmazonECS/latest/developerguide/container-metadata.html)
 * for details.
 *
 * @param {Object} [options] - configuration options
 * @param {string} options.taskFile - name of the ecs metadata file. defaults to `process.env.ECS_CONTAINER_METADATA_FILE`
 * @returns {Promise<ECSFileTaskInformation>} information about the current docker process on ecs
 */
function ecsCurrentTaskFromFile({ taskFile } = { taskFile: process.env.ECS_CONTAINER_METADATA_FILE }) {
  return readFile(taskFile, 'utf8')
    .then(fileContent => JSON.parse(fileContent))
    .then(taskInformation => ({
      cluster: taskInformation.Cluster,
      arn: taskInformation.TaskARN,
      dockerId: taskInformation.ContainerID,
      containerName: taskInformation.ContainerName,
      ports: (taskInformation.PortMappings || []).map(ports => ({
        protocol: ports.Protocol,
        container: ports.ContainerPort,
        host: ports.HostPort,
        bindIp: ports.BindIp
      }))
    }));
}

module.exports = ecsCurrentTaskFromFile;
