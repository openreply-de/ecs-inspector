/**
 * @file retrieves the cluster name from an instances ecs agent
 * @author Florian Schaper <f.schaper@reply.de>
 * @copyright Florian Schaper 2018, MIT License
 */

'use strict';

const request = require('request-promise-native');

/**
 * @typedef {Object} ECSAgentClusterName
 * @property {string} cluster - name of the ECS cluster
 */

/**
 * retrieves the name of the cluster this ecs task run's on
 *
 * @param {Object} [options] - configuration options
 * @param {string} options.ecsAgentHost - hostname and port of the ecs agent. defaults to `172.17.0.1:51678`
 * @param {number} options.timeout - milliseconds to wait for the discovery service to respond on each request
 * @returns {Promise<ECSAgentClusterName>} cluster name
 */
function ecsAgentClusterName({ ecsAgentHost, timeout } = { ecsAgentHost: '172.17.0.1:51678', timeout: 1000 }) {
  return request.get(`http://${ecsAgentHost}/v1/metadata`, { json: true, timeout })
    .then(agentInformation => ({ cluster: agentInformation.Cluster }));
}

module.exports = ecsAgentClusterName;
