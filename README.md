# AWS ECS Inspection

this node module allows you to retrieve information about an ecs instance for the current process.

During discovery this module will perform several http calls against the local ecs agent and the ec2 metadata service. AWS throttles queries to the instance metadata service on a per-instance basis and places limits on the number of simultaneous connections from an instance to the instance metadata service.

## Getting Started

### Installing

you can install this package via npm

`npm i @openreply/ecs-inspector`

### Usage

```javascript
const inspector = require('@openreply/ecs-inspector');
// using the promise interface
inspector().then( result => console.log(result) ).catch( error => console.error(error.message) );
// using callbacks
inspector({callback: (err, result) => {
  if (err) {
    return console.error(err.message);
  }
  console.log(result);
}});
```

Example result

```javascript
{
  cluster: 'default',
  arn: 'arn:aws:ecs:eu-central-1:123456789012:task/9c621769-1a09-42c7-8d5d-2ba3319682c7',
  dockerId: '2418684f95999553475079f1c6845f5b6cc99a057cbe680a82ad3396db799542',
  containerName: 'service-b',
  ports: [{
    protocol: 'tcp',
    container: 3000,
    host: 36092
  }],
  publicHostname: 'some-host.example.com',
  publicIpv4: '134.102.200.14',
  localHostname: 'private-hostname.internal',
  localIpv4: '10.0.1.10'
}
```
## ECSTaskInformationPortInfo : Object
**Kind**: global typedef  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| protocol | string | protocol used e.g. tcp |
| container | number | port on the container side |
| host | number | port on the host side |

## ECSTaskInformation : Object
**Kind**: global typedef  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| arn | string | aws arn uniquely identifying the task |
| cluster | string | name of the cluster |
| dockerId | string | docker id |
| containerName | string | name of the container |
| publicHostname | string | public dns domain name of the ec2 instance (if available) |
| publicIpv4 | string | public ipv4 address of the ec2 instance (if available) |
| localHostname | string | private dns domain name of the ec2 instance (if available) |
| localIpv4 | string | private ipv4 address of the ec2 instance (if available) |
| ports | Array.<ECSTaskInformationPortInfo> | docker to host port mapping |


See the [API documentation](API.md) for more details.


## Running the tests

You can run the test suite via

`npm test`

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://bitbucket.org/openreply-de/emw-request-id). 

## Authors

* **Florian Schaper** - *Initial work* - [openreply-de](https://bitbucket.org/openreply-de/emw-request-id)

See also the list of [authors](AUTHORS.md) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## References

- [ecs agent inspection documentation](https://docs.aws.amazon.com/AmazonECS/latest/developerguide/ecs-agent-introspection.html)
- [ecs metadata file documentation](https://docs.aws.amazon.com/AmazonECS/latest/developerguide/container-metadata.html)
- [ec2 metadata api](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ec2-instance-metadata.html)
