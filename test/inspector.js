'use strict';

const sampleMetaData = require('./testdata/webserver/v1/metadata.json');
const sampleThreadData = require('./testdata/webserver/v1/threads.json');
const sampleMetaDataFile = require('./testdata/filesystem/metadata-file.json');

const sinon = require('sinon');

// mock file system access globally before including inspect
const fs = require('fs');

const readFileStub = sinon.stub(fs, 'readFile');
readFileStub.withArgs('/proc/1/cpuset').callsArgWith(2, null, '/ecs/9c621769-1a09-42c7-8d5d-2ba3319682c7/2418684f95999553475079f1c6845f5b6cc99a057cbe680a82ad3396db799542');
readFileStub.withArgs('/ecs/metadata').callsArgWith(2, null, JSON.stringify(sampleMetaDataFile));
readFileStub.callsArgWith(2, new Error('this file has not been mocked'));

const chai = require('chai');
const nock = require('nock');

const chaiAsPromised = require('chai-as-promised');

chai.use(chaiAsPromised);

const { expect } = chai;

const inspect = require('..');

function mockEc2InstanceDiscovery(onlyPartially = false) {
  if (!nock.isActive()) {
    nock.activate();
  }

  const mapping = {
    'local-hostname': 'private-hostname.internal',
    'local-ipv4': '10.0.1.10'
  };
  if (!onlyPartially) {
    mapping['public-hostname'] = 'some-host.example.com';
    mapping['public-ipv4'] = '134.102.200.14';
  }

  return Object.entries(mapping).map(([path, response]) => nock('http://169.254.169.254').get(`/latest/meta-data/${path}`).reply(200, response));
}

function mockEcsAgentDiscovery() {
  if (!nock.isActive()) {
    nock.activate();
  }
  return nock('http://172.17.0.1:51678')
    .get('/v1/metadata').reply(200, sampleMetaData)
    .get('/v1/threads')
    .reply(200, sampleThreadData);
}

function unmockHttp() {
  nock.cleanAll();
  nock.restore();
}

describe('inspector', () => {
  it('should return a rejected promise if no ecs and ec2 information can be retrieved', () => expect(inspect()).to.eventually.be.rejected).timeout(5000);

  describe('ec2 instance discovery and ECS_CONTAINER_METADATA_FILE', () => {
    beforeEach(() => {
      process.env.ECS_CONTAINER_METADATA_FILE = '/ecs/metadata';
      mockEc2InstanceDiscovery();
    });

    it('should return ec2 instance and task file information\'s if available', () => expect(inspect()).to.eventually.eql({
      cluster: 'default',
      arn: 'arn:aws:ecs:us-west-2:012345678910:task/2b88376d-aba3-4950-9ddf-bcb0f388a40c',
      dockerId: '98e44444008169587b826b4cd76c6732e5899747e753af1e19a35db64f9e9c32',
      containerName: 'metadata',
      ports: [{
        protocol: 'tcp',
        container: 80,
        host: 80,
        bindIp: ''
      }],
      publicHostname: 'some-host.example.com',
      publicIpv4: '134.102.200.14',
      localHostname: 'private-hostname.internal',
      localIpv4: '10.0.1.10'
    })).timeout(5000);

    afterEach(() => {
      unmockHttp();
      delete process.env.ECS_CONTAINER_METADATA_FILE;
    });
  });

  describe('ec2 instance discovery but no task file or agent access', () => {
    beforeEach(() => {
      mockEc2InstanceDiscovery();
    });

    it('should return a rejected promise', () => expect(inspect()).to.eventually.be.rejected).timeout(5000);

    it('should call a passed callbacks error handler', (done) => {
      inspect({
        callback: (error) => {
          expect(error).to.be.an.instanceof(Error);
          done();
        }
      });
    }).timeout(5000);

    afterEach(() => {
      unmockHttp();
    });
  });

  describe('ec2 instance discovery, no task file but agent access', () => {
    beforeEach(() => {
      mockEc2InstanceDiscovery();
      mockEcsAgentDiscovery();
    });

    it('should respond with ec2 instance and task information', () => expect(inspect()).to.eventually.deep.equal({
      cluster: 'default',
      arn: 'arn:aws:ecs:eu-central-1:123456789012:task/9c621769-1a09-42c7-8d5d-2ba3319682c7',
      dockerId: '2418684f95999553475079f1c6845f5b6cc99a057cbe680a82ad3396db799542',
      containerName: 'service-b',
      ports: [{
        protocol: 'tcp',
        container: 3000,
        host: 36092
      }],
      publicHostname: 'some-host.example.com',
      publicIpv4: '134.102.200.14',
      localHostname: 'private-hostname.internal',
      localIpv4: '10.0.1.10'
    })).timeout(5000);

    it('should call a passed callbacks result handler', (done) => {
      inspect({
        callback: (error, response) => {
          expect(error).to.eql(null);
          expect(response).to.deep.eql({
            cluster: 'default',
            arn: 'arn:aws:ecs:eu-central-1:123456789012:task/9c621769-1a09-42c7-8d5d-2ba3319682c7',
            dockerId: '2418684f95999553475079f1c6845f5b6cc99a057cbe680a82ad3396db799542',
            containerName: 'service-b',
            ports: [{
              protocol: 'tcp',
              container: 3000,
              host: 36092
            }],
            publicHostname: 'some-host.example.com',
            publicIpv4: '134.102.200.14',
            localHostname: 'private-hostname.internal',
            localIpv4: '10.0.1.10'
          });
          done();
        }
      });
    }).timeout(5000);

    afterEach(() => {
      unmockHttp();
    });
  });

  describe('ec2 instance discovery but no matching task in task list', () => {
    beforeEach(() => {
      mockEc2InstanceDiscovery();
      mockEcsAgentDiscovery();
      // this call does not map to any task from our task list
      readFileStub.withArgs('/proc/1/cpuset').callsArgWith(2, null, '/ecs/00000000-0000-0000-0000-000000000000/2418684f95999553475079f1c6845f5b6cc99a057cbe680a82ad3396db799542');
    });

    it('return a rejected promise', () => expect(inspect()).to.eventually.be.rejected);

    afterEach(() => {
      readFileStub.withArgs('/proc/1/cpuset').callsArgWith(2, null, '/ecs/9c621769-1a09-42c7-8d5d-2ba3319682c7/2418684f95999553475079f1c6845f5b6cc99a057cbe680a82ad3396db799542');
      unmockHttp();
    });
  });

  describe('partial ec2 instance discovery', () => {
    before(() => {
      mockEc2InstanceDiscovery(true);
      mockEcsAgentDiscovery();
      // this call does not map to any task from our task list
    });

    it('should handle partial ec2 instance discovery', () => expect(inspect()).to.eventually.deep.equal({
      cluster: 'default',
      arn: 'arn:aws:ecs:eu-central-1:123456789012:task/9c621769-1a09-42c7-8d5d-2ba3319682c7',
      dockerId: '2418684f95999553475079f1c6845f5b6cc99a057cbe680a82ad3396db799542',
      containerName: 'service-b',
      ports: [{
        protocol: 'tcp',
        container: 3000,
        host: 36092
      }],
      localHostname: 'private-hostname.internal',
      localIpv4: '10.0.1.10'
    }));

    after(() => {
      unmockHttp();
    });
  });
});
