
## inspect([options]) ⇒ Promise.<ECSTaskInformation>
provides information about the ec2/ecs instance that a docker image is currently running from

**Kind**: global function  
**Returns**: Promise.<ECSTaskInformation> - information about the current docker process on ecs  

| Param | Type | Description |
| --- | --- | --- |
| [options] | Object | additional options |
| [options.callback] | InspectHandler | optional callback which will be called with the result or an error |

## ECSTaskInformationPortInfo : Object
**Kind**: global typedef  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| protocol | string | protocol used e.g. tcp |
| container | number | port on the container side |
| host | number | port on the host side |

## ECSTaskInformation : Object
**Kind**: global typedef  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| arn | string | aws arn uniquely identifying the task |
| cluster | string | name of the cluster |
| dockerId | string | docker id |
| containerName | string | name of the container |
| publicHostname | string | public dns domain name of the ec2 instance (if available) |
| publicIpv4 | string | public ipv4 address of the ec2 instance (if available) |
| localHostname | string | private dns domain name of the ec2 instance (if available) |
| localIpv4 | string | private ipv4 address of the ec2 instance (if available) |
| ports | Array.<ECSTaskInformationPortInfo> | docker to host port mapping |

## InspectHandler : function
**Kind**: global typedef  

| Param | Type | Description |
| --- | --- | --- |
| error | Error ⎮ null | error that may have occurred |
| result | ECSTaskInformation | result of the operation |

